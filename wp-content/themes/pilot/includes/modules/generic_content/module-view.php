<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="gc-wrap ">
	<h3><?php echo $args['title']; ?></h3>
	<div class="gc-content"><?php echo $args['content']; ?></div>
</div>
