<?php 
	/**
	 * string	$args['title']
	 */
	global $args; 
?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
<div class="way-one-container">
	<div class="bg-cloud" id="waypoint-one">
	</div>
	<div class="bg-cloud-two" id="waypoint-target">
		<div class="container">
			<div class="mobile-wordmark-wrapper">
				<?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/joinerywhite.svg'); ?>
			</div>

			<div class="jlogo-wrapper">
				<?php for ($i = 1; $i <= 1; $i++) : ?>
					<img class="letter" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
					<img class="letter" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<?php endfor; ?>
				<!--<img class="wordmark" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/joinery.svg">-->
				<a href="/">
					<div class="wordmark-wrapper">
						<img id="l1" class="letter holder" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
				<div id="w1" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/lessthan.svg'); ?></div>
						<img id="l2" class="letter holder" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<div id="w2" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/slash.svg'); ?></div>
						<img id="l3" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
				<div id="w3" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/jay.svg'); ?></div>
						<img id="l4" class="letter holder" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<div id="w4" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/one.svg'); ?></div>
						<img id="l5" class="letter holder" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
				<div id="w5" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/oh.svg'); ?></div>
						<img id="l6" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<div id="w6" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/en.svg'); ?></div>
						<img id="l7" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
				<div id="w7" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/ee.svg'); ?></div>
						<img id="l8" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<div id="w8" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/ar.svg'); ?></div>
						<img id="l9" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
				<div id="w9" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/why.svg'); ?></div>
						<img id="l10" class="letter " src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<div id="w10" class="wordmark"><?php echo file_get_contents(get_template_directory_uri().'/includes/modules/joinery/img/greaterthan.svg'); ?></div>
					</div>
				</a>
				<?php for ($i = 1; $i <= 5; $i++) : ?>
					<img class="letter" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/one.svg">
					<img class="letter" src="<?php echo get_template_directory_uri(); ?>/includes/modules/joinery/img/oh.svg">
				<?php endfor; ?>
			</div>
		</div>
		<div>
			<div class="hero-text" id="hero-tag">Web Developers for Sites and Apps
			</div>
		</div>
	</div>
</div>