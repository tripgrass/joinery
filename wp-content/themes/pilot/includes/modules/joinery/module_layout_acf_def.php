<?php
	global $pilot;
	$module = "joinery";
	// add module layout to flexible content 
	$module_layout = array (
		'key' => create_key($module),
		'name' => 'joinery_block',
		'label' => 'Joinery Logo Block',
		'display' => 'block',
		'sub_fields' => array (
		),
		'min' => '',
		'max' => '',
	);
?>