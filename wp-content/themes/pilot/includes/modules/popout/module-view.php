<?php
	/**
	 * array	$args['images']
	 * img url	$args['images'][0]['url']			// thumbnail url 
	 * string	$args['images'][0]['title'] 
	 * string	$args['images'][0]['subtitle'] 
	 * string	$args['images'][0]['popup_content'] 
	 */
	global $args; 
?>
<div class="image-popup-wrap block-image-popup">
	<?php if( count($args['images']) > 0 ) : ?>
		<div>
			<?php if( is_array( $args['images'] ) ) : ?>
				<?php foreach( $args['images'] as $key => $image ): ?>
					<div class="slide-wrapper">
						<div class="slide">
							<div class="bg-image" style="background-image: url(<?php echo $image['url']; ?>)"></div>
							
							<?php if( $image['title'] || $image['subtitle'] ) : // if has text, add text box ?>
								<div class="slide-text">
									<span class="title"><?php echo $image['title']; ?></span>
									<span class="subtitle"><?php echo $image['subtitle']; ?></span>					
								</div>
							<?php endif; ?>
						</div>
					</div>
					<?php if($image['popup_content']) : ?>
						<div class="slide-content">
							<?php echo $image['popup_content']; ?>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
