var allSlideContent = $('.image-popup-wrap .slide-content'),
      allSlides = $('.image-popup-wrap .slide-wrapper');

  allSlides.on('click', function () {
    slideContent = $(this).next('.slide-content');

    if ( !slideContent.hasClass('active') ) {
      allSlideContent.slideUp().removeClass('active');
      allSlides.removeClass('active');
      slideContent.slideDown().addClass('active');
      $(this).addClass('active');
    } else {
      slideContent.slideUp().removeClass('active');
      $(this).removeClass('active');
    }
  });
