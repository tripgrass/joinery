<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_three_column_layout(){
		$args = array(
			'title' => get_sub_field('three_column_block_title'),
			'columns' => get_sub_field('three_column_block_columns'),
		);
		return $args;
	}
?>