<?php 
	/**
	 * string	$args['title']
	 * string	$args['columns']
	 */
	global $args;
?>
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700" rel="stylesheet">
<div class="three-col-wrap">
	<div class="container">
		<div class="header-wrap">
			<h2>What We Can Do For You</h2>
		</div>
		<?php $i = 1; foreach( $args['columns'] as $col ) : ?>
			<div class="column-wrap">
				<div id="col-<?php echo $i; ?>" style="background-image: url(<?php echo $col['icon']['url']; ?>);">
				</div>
					<h3><?php echo $col['title']; ?></h3>
				<div class="three-col-content"><?php echo $col['text']; ?></div>
			</div>
		<?php $i++; endforeach; ?>
	</div>
</div>
