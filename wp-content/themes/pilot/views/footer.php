<footer class="site-footer">
	<div class="site-info">
		<?php echo date("Y"); ?>&copy; <?php printf( esc_html__( 'Design by %1$s', 'pilot' ), '<a href="http://sonderagency.com/">Sonder</a>' ); ?>
	</div><!-- .site-info -->
</footer>